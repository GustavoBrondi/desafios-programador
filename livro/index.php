<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <script>
    function mostrarResultado(str) {
        if (str.length == 0) {
            document.getElementById("divbuscar").innerHTML = "";
            document.getElementById("divbuscar").style.border = "0px";            
            return;
        }
        if (window.XMLHttpRequest) {

            xmlhttp = new  XMLHttpRequest();
            //ie7+, firefox, chrome ,opera e safari
        } else {

            xmlhttp = new  ActiveXObject("Microsoft.XMLHTTP");
            // ie6 e Ie5
        }

        xmlhttp.onreadystatechange = function(){

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                document.getElementById("divbuscar").innerHTML = xmlhttp.responseText;
                document.getElementById("divbuscar").style.border = "1px solid #a5acb2"; 

                }
            }
            xmlhttp.open("GET", "buscar.php?q="+ str, true);
            xmlhttp.send();
        
    }
    
    
    </script>


</head>
<body>
    <?php include("menu.php") ?>
    
    <h1 class='livrovisitas'>Livro de visitas</h1>
    <fieldset>
    <legend>Efetue o cadastro:</legend>
    <form id="form1" name="form1" method="post" action="valida.php">
    <p>
    <div>
    <label for="nome">Nome:</label>
    </div>
    <input type="text" name="nome" id="nome" required placeholder="Digite seu nome" />
    </p>

    <p>
    <div>
    <label for="endereco">Endereço:</label>
    </div>
    <input type="text" name="endereco" id="endereco" required placeholder="Digite seu endereço"/>
    </p>

    <p>
    <div>
    <label for="telefone">Telefone: </label>
    </div>
    <input type="text" name="telefone" id="telefone" required placeholder="Digite seu numero de telefone"/>
    </p>

    <p>
    <div>
    <label for="email">Email: </label>
    </div>
    <input type="email" name="email" id="email" required placeholder="Digite seu email"/>
    </p>

    <p>
    <input type="submit" name="enviar" id="enviar" value="Enviar" class="button" />
    </p>

   
    
    </form>
    </fieldset>

    <h2>Escreva sua consulta...</h2>
    <form name="frm_consulta" id="frm_consulta">
        <input onkeyup="mostrarResultado(this.value)" type="text" placeholder="Digite um nome..." name="busca" id="busca" />
    </form>


    <h3>Clientes Cadastrados</h3>

    <?php
        include("conecta.php");
        $sql = "SELECT nome_visita, endereco_visita, telefone_visita, email_visita, cod_visita FROM tb_visitas";
        $res = mysqli_query($_con, $sql);

        print "<div id='divbuscar'>"; 

print "
    <table width='50%' border='0'>
        <tr>
            <th class='tabela'>Nome</th>
            <th class='tabela'>Endereço</th>
            <th class='tabela'>Telefone</th>
            <th class='tabela'>E-mail</th>    
            <th class='tabela'>Editar</th>    
            <th class='tabela'>Apagar</th>    
        </tr>
";

while ($linha = mysqli_fetch_array($res)){

    print "
        <tr class='marca_linha'> 
            <td class='tabela'>$linha[0]</td>
            <td class='tabela'>$linha[1]</td>
            <td class='tabela'>$linha[2]</td>
            <td class='tabela'>$linha[3]</td>
            <td class='tabela'><a href='editar.php?cod=$linha[4]'>Editar</a></td>
            <td class='tabela'><a href='apagar.php?cod=$linha[4]'>Apagar</a></td>
        </tr>
    
    ";

}

print "</table> </div>";

mysqli_close($_con);

?>

</body>
</html>