-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 03-Mar-2020 às 21:11
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bd_aula`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_cidade`
--

CREATE TABLE `tb_cidade` (
  `cod_cidade` int(11) NOT NULL,
  `cidade_nome` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `cidade_data` date NOT NULL,
  `cidade_hora` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_cidade`
--

INSERT INTO `tb_cidade` (`cod_cidade`, `cidade_nome`, `cidade_data`, `cidade_hora`) VALUES
(22, 'Pindamonhangaba', '2020-02-28', '14:12:53'),
(31, 'Taubate', '2020-03-03', '15:43:29'),
(32, 'Aparecida', '2020-03-03', '15:43:31'),
(33, 'Taubateeeee', '2020-03-03', '15:43:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_visitas`
--

CREATE TABLE `tb_visitas` (
  `cod_visita` int(11) NOT NULL,
  `nome_visita` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `endereco_visita` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `telefone_visita` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email_visita` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `data_visita` date NOT NULL,
  `hora_visita` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_visitas`
--

INSERT INTO `tb_visitas` (`cod_visita`, `nome_visita`, `endereco_visita`, `telefone_visita`, `email_visita`, `data_visita`, `hora_visita`) VALUES
(7, 'Gustavo', 'Rua dos Antúrios 261', '12 991278-5652', 'gustavoobrondi@gmail.com', '2020-02-28', '17:15:56'),
(17, 'Gabriel', 'Rua Thomas M', '12 998835-6950', 'GGbiel@gmail.com', '2020-03-03', '14:09:47'),
(18, 'Gustavo', 'Rua dos Antúrios 261', '12 991278-5652', 'gustavoobrondi@gmail.com', '2020-03-03', '15:33:44'),
(19, 'Gustavo', 'Rua dos Antúrios 261', '12 991278-5652', 'gustavoobrondi@gmail.com', '2020-03-03', '15:33:48');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `tb_cidade`
--
ALTER TABLE `tb_cidade`
  ADD PRIMARY KEY (`cod_cidade`);

--
-- Índices para tabela `tb_visitas`
--
ALTER TABLE `tb_visitas`
  ADD PRIMARY KEY (`cod_visita`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `tb_cidade`
--
ALTER TABLE `tb_cidade`
  MODIFY `cod_cidade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de tabela `tb_visitas`
--
ALTER TABLE `tb_visitas`
  MODIFY `cod_visita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
