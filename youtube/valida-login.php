<?php 
session_start();

if ((empty($_POST['email'])) ||  (empty($_POST['senha']))) {
    print "Verifique todos os campos. <a href='novo-usuario.php'>Voltar</a>'";
    exit;
}

include ("conecta.php");
include ("funcao.php");

$email = LimpaString($_POST['email']);
$senha = LimpaString($_POST['senha']);

$sql = "SELECT COUNT(cod_usuario), cod_usuario, nome_usuario, email_usuario, senha_usuario, foto_usuario
FROM tb_usuario WHERE email_usuario = '$email'";


$res = mysqli_query($_con, $sql) or die("Não foi possivel fazer o login. ERRO 31 ");

$linha = mysqli_fetch_array($res);

if ($linha[0] > 0) {
    if (password_verify($senha, $linha[4])) {
        $_SESSION['cod_usuario'] = $linha[1];
        $_SESSION['nome_usuario'] = $linha[2];
        $_SESSION['email_usuario'] = $linha[3];
        $_SESSION['foto_usuario'] = $linha[5];
        header("location: index.php");
    } else {
        header("location: login.php");
    }
} else {
    header("location: login.php");
}

mysqli_close($_con);