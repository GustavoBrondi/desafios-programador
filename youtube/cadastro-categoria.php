<?php
session_start();
include("verifica-logado.php");

?>

<!DOCTYPE html>
<html>

<head>
    <title>Cadastro de Categorias</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width inicial-scale=1.0">
    <link href="style.css" rel="stylesheet" type="text/css" />

    <script>
        function mostrarResultado(str) {
            if (str.length == 0) {
                document.getElementById("divbuscar").innerHTML = "";
                document.getElementById("divbuscar").style.border = "0px";
                return;
            }
            if (window.XMLHttpRequest) {

                xmlhttp = new XMLHttpRequest();
                // ie7+, firefox, Chrome, Opera e safari
            } else {

                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                // ie6 e ie5
            }

            xmlhttp.onreadystatechange = function() {

                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("divbuscar").innerHTML = xmlhttp.responseText;
                    document.getElementById("divbuscar").style.border = "1px solid #a5acb2";

                }
            }
            xmlhttp.open("GET", "buscar-categoria.php?q=" + str, true);
            xmlhttp.send();
        }
    </script>

</head>

<body>
    <?php include("menu.php"); ?>
    <h1>Cadastro de Categorias</h1>
    <fieldset>
        <legend> Efetue o cadastro da Categoria:</legend>
        <form id="form1" name="form1" method="post" action="valida-categoria.php">
            <p>Nome:<br />
                <input name="nome_categoria" required placeholder="Entre com o nome da categoria" id="nome_categoria" type="text" />
            </p>
            <p>
                <input name="enviar" id="enviar" value="Enviar" type="submit" />
            </p>
        </form>
    </fieldset>

    <h2>Escreva sua consulta...</h2>
    <form name="frm_consulta" id="frm_consulta">
        <input onkeyup="mostrarResultado(this.value)" name="busca" id="busca" type="text" placeholder="Digite um nome..." />
    </form>

    <h3>Categorias Cadastradas</h3>

    <?php
    include("conecta.php");
    $sql = "SELECT nome_categoria, cod_categoria FROM tb_categoria";
    $res = mysqli_query($_con, $sql);

    print "<div id='divbuscar'>";

    print "
<table width='100%' border='0'>
<tr>
<th class='tabela'>Nome Cidade</th>
<th class='tabela'>Editar</th>
<th class='tabela'>Apagar</th>
</tr>
";
    while ($linha = mysqli_fetch_array($res)) {
        print "
      <tr class='marca_linha'>
         <td class='tabela'>$linha[0]</td>
         <td class='tabela'><a href='editar-categoria.php?cod=$linha[1]'>Editar</a></td>
         <td class='tabela'><a href='apagar-categoria.php?cod=$linha[1]'>Apagar</a></td>
      </tr>
   ";
    }

    print "</table></div>";

    mysqli_close($_con);

    ?>


</body>

</html>