const axios = require('axios');

axios.get("http://senacao.tk/objetos/computador_array_objeto")
.then(function(res) {
    const computador = res.data;
    console.log(`Marca: ${computador.marca}`);
    console.log(`Modelo: ${computador.modelo}`);
    console.log(`Memoria: ${computador.memoria}`);
    console.log(`SSD: ${computador.ssd}`);
    console.log(`Processador: ${computador.processador}`);
    
    console.log(`-------------`);

    computador.softwares.forEach(function(softwares) {
        console.log(`softwares: ${softwares}`);

    })

    console.log(`-------------`);

    console.log(`rsocial: ${computador.fornecedor.rsocial}`);
    console.log(`Telefone: ${computador.fornecedor.telefone}`);
    console.log(`Endereço: ${computador.fornecedor.endereco}`);

});