const axios = require('axios');

axios.get("http://senacao.tk/objetos/veiculo_array_objeto")
.then(function(res) {
    const carro = res.data;

    console.log(`Marca: ${carro.marca}`);
    console.log(`Modelo: ${carro.modelo}`);
    console.log(`Ano: ${carro.ano}`);
    console.log(`Quilometragem: ${carro.quilometragem}`);

    console.log(`------Opcionais------`);

    carro.opcionais.forEach(function(opcionais, index) {
        console.log(`Opcionais ${index+1}: ${opcionais}`)
    })

    console.log(`------Vendedor------`);

    console.log(`Nome: ${carro.vendedor.nome}`);
    console.log(`Idade: ${carro.vendedor.idade}`);
    console.log(`Celular: ${carro.vendedor.celular}`);
    console.log(`Cidade: ${carro.vendedor.cidade}`);

});