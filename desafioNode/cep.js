const axios = require(`axios`)

axios.get("https://viacep.com.br/ws/01001000/json/")
.then(function(res){
    const viacep = res.data;

    console.log(`Cep: ${viacep.cep}`);
    console.log(`Logradouro: ${viacep.logradouro}`);
    console.log(`Complemento: ${viacep.complemento}`);
    console.log(`Bairro: ${viacep.bairro}`);
    console.log(`Localidade: ${viacep.localidade}`);
    console.log(`Uf: ${viacep.uf}`);
    console.log(`Unidade: ${viacep.unidade}`);
    console.log(`Ibge: ${viacep.ibge}`);
    console.log(`Gia: ${viacep.gia}`);
    

})