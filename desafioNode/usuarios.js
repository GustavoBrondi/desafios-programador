const axios = require('axios');

axios.get("http://senacao.tk/objetos/usuario")
.then(function(res) {
    const user = res.data;

    console.log(`                      `);
    console.log(`----Dados usuario----`);
    console.log(`                      `);
   
    console.log(`Nome: ${user.nome}`);
    console.log(`Idade: ${user.email}`);
    console.log(`Cidade: ${user.telefone}`);

    console.log(`                      `);
    console.log(`----Conhecimentos----`);
    console.log(`                      `);

    user.conhecimentos.forEach(function(conhecimentos, index) {
        console.log(`Conhecimentos ${index+1}: ${conhecimentos}`);

    })

    console.log(`                      `);
    console.log(`----Endereço----`);
    console.log(`                      `);

    console.log(`Rua: ${user.endereco.rua}`);
    console.log(`Numero: ${user.endereco.numero}`);
    console.log(`Bairro: ${user.endereco.bairro}`);
    console.log(`Cidade: ${user.endereco.cidade}`);
    console.log(`UF: ${user.endereco.uf}`);

    console.log(`                      `);
    console.log(`----Qualificações----`);
    console.log(`                      `);
    
    user.qualificacoes.forEach(function(qualificacoes) {
        console.log(`Nome: ${qualificacoes.nome}`);
        console.log(`Instituição: ${qualificacoes.instituicao}`);
        console.log(`Ano: ${qualificacoes.ano}`);
        console.log(`---------`);
        
    })

});