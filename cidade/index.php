<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../livro/style.css">

    <script>
        function mostrarResultado(str) {
        if (str.length == 0) {
            document.getElementById("divbuscar").innerHTML = "";
            document.getElementById("divbuscar").style.border = "0px";            
            return;
        }
        if (window.XMLHttpRequest) {

            xmlhttp = new  XMLHttpRequest();
            //ie7+, firefox, chrome ,opera e safari
        } else {

            xmlhttp = new  ActiveXObject("Microsoft.XMLHTTP");
            // ie6 e Ie5
        }

        xmlhttp.onreadystatechange = function(){

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                document.getElementById("divbuscar").innerHTML = xmlhttp.responseText;
                document.getElementById("divbuscar").style.border = "1px solid #a5acb2"; 

                }
            }
            xmlhttp.open("GET", "buscarCidade.php?q="+ str, true);
            xmlhttp.send();
        
    }
    
    </script>


</head>
<body>
<?php include("../livro/menu.php"); ?>



    <h1 class="livrovisitas">Livro de visitas</h1>
    <fieldset>
    <legend>Efetue o cadastro:</legend>
    <form id="form1" name="form1" method="post" action="validaCidade.php">
    <p>
    <div>
    <label for="cidade">Cidade:</label>
    </div>
    <input type="text" name="cidade" id="cidade" required placeholder="Digite o nome da sua cidade" />
    </p>

    <p>
    <input type="submit" name="enviar" id="enviar" value="Enviar" class="button" />
    </p>


    
    </form>
    </fieldset>

    <h2>Escreva sua consulta...</h2>
    <form name="frm_consulta" id="frm_consulta">
        <input onkeyup="mostrarResultado(this.value)" type="text" placeholder="Digite um nome..." name="busca" id="busca" />
    </form>

<h3>Cidades Cadastrados</h3>

<?php
        include("conecta.php");
        $sql = "SELECT cidade_nome, cod_cidade FROM tb_cidade";
        $res = mysqli_query($_con, $sql);

        print "<div id='divbuscar'>";

print "
    <table width='50%' border='0'>
        <tr>
            <th class='tabela'>Nome Cidade:</th>
            <th class='tabela'>Editar</th>    
            <th class='tabela'>Apagar</th> 
            
        </tr>
";

while ($linha = mysqli_fetch_array($res)){

    print "
        <tr class='marca_linha'> 
            <td class='tabela'>$linha[0]</td>
            <td class='tabela'><a href='editarCidade.php?cod=$linha[1]'>Editar</a></td>
            <td class='tabela'><a href='apagarCidade.php?cod=$linha[1]'>Apagar</a></td>
           
        </tr>
    
    ";

}

print "</table>";

mysqli_close($_con);

?>




</body>
</html>