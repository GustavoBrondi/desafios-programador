<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aplicação video</title>
    <link rel="stylesheet" href="style.css">

    <script>
    function mostrarResultado(str) {
        if (str.length == 0) {
            document.getElementById("videobuscar").innerHTML = "";
            document.getElementById("videobuscar").style.border = "0px";            
            return;
        }
        if (window.XMLHttpRequest) {

            xmlhttp = new  XMLHttpRequest();
            //ie7+, firefox, chrome ,opera e safari
        } else {

            xmlhttp = new  ActiveXObject("Microsoft.XMLHTTP");
            // ie6 e Ie5
        }

        xmlhttp.onreadystatechange = function(){

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                document.getElementById("videobuscar").innerHTML = xmlhttp.responseText;
                document.getElementById("videobuscar").style.border = "1px solid #a5acb2"; 

                }
            }
            xmlhttp.open("GET", "buscar.php?q="+ str, true);
            xmlhttp.send();
        
    }
    
    
    </script>
</head>
<body>
<?php include ("menu.php"); ?>

        <h2>Aplicação Video</h2>

<fieldset>
        

        <legend>Procure o video:</legend>
        <form id="form1" name="form1" method="post" action="validaVideo.php">
        <p>
            <label for="nomevideo" name = "nomevideo">Nome do video:</label>
            <input type="text" required name = "nomevideo" id = "nomevideo" placeholder = "Insira o nome do video"> 
        </p>
        <p>
            <label for="idvideo" name = "idvideo">ID do video</label>
            <input type="text" required name = "idvideo" id = "idvideo" placeholder = "Insira o id do video"> 
        </p>

        <p>
            <input type="submit" name = "enviar" id ="enviar" value = "Enviar">
        </p>
</form>

    </fieldset>

    <h2>Escreva sua consulta...</h2>
    <form name="frm_consulta" id="frm_consulta">
        <input onkeyup="mostrarResultado(this.value)" type="text" placeholder="Digite um nome..." name="busca" id="busca" />
    </form>

    <h3>Videos Cadastrados</h3>

<?php
    include("conecta.php");
    $sql = "SELECT nome_video, id_video, cod_video FROM tb_videos";
    $res = mysqli_query($_con, $sql);

    print "<div id='videobuscar'>"; 

print "
<table width='50%' border='0'>
    <tr>
        <th class='tabela'>Nome</th>
        <th class='tabela'>ID</th>  
        <th class='tabela'>Editar</th>    
        <th class='tabela'>Apagar</th>    
    </tr>
";

while ($linha = mysqli_fetch_array($res)){

print "
    <tr class='marca_linha'> 
        <td class='tabela'>$linha[0]</td>
        <td class='tabela'> <a target='blank' href='video.php?cod=$linha[2]'> <img src='https://i.ytimg.com/vi/$linha[1]/hqdefault.jpg' /> 
        <a/></td>
        <td class='tabela'><a href='editar.php?cod=$linha[2]'>Editar</a></td>
        <td class='tabela'><a href='apagar.php?cod=$linha[2]'>Apagar</a></td>
    </tr>

";

}

print "</table> </div>";


?>

</body>
</html>