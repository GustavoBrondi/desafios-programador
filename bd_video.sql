-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 12-Mar-2020 às 21:23
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bd_video`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categoria`
--

CREATE TABLE `tb_categoria` (
  `cod_categoria` int(11) NOT NULL,
  `nome_categoria` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `data_categoria` date NOT NULL,
  `hora_categoria` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_categoria`
--

INSERT INTO `tb_categoria` (`cod_categoria`, `nome_categoria`, `data_categoria`, `hora_categoria`) VALUES
(5, 'Stand UP', '2020-03-05', '14:59:49'),
(6, 'Musica', '2020-03-05', '15:19:16'),
(7, 'Jogos', '2020-03-05', '16:47:07'),
(10, 'Terror', '2020-03-11', '16:48:54'),
(12, 'Noticias', '2020-03-11', '17:17:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuario`
--

CREATE TABLE `tb_usuario` (
  `nome_usuario` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email_usuario` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `senha_usuario` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `foto_usuario` varchar(300) COLLATE latin1_general_ci NOT NULL,
  `data_usuario` date NOT NULL,
  `hora_usuario` time NOT NULL,
  `cod_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_usuario`
--

INSERT INTO `tb_usuario` (`nome_usuario`, `email_usuario`, `senha_usuario`, `foto_usuario`, `data_usuario`, `hora_usuario`, `cod_usuario`) VALUES
('Gustavo', 'gustavoobrondi@gmail.com', '$2y$10$GQtPRgQ3RJUX9avG7kCKiuQgJAP8ZJ6I7KWkP4NGcUJArjw8V0BGa', 'upload/avatar.png', '2020-03-10', '17:23:58', 1),
('Biel', 'GGbiel@gmail.com', '$2y$10$SEpQYHbNUSWd0/L2NWjzseOqU7StdwBvbEBK922DBZceA73JWif4K', 'upload/avatar.png', '2020-03-10', '17:24:34', 2),
('Gustavo', 'gustavo@gmail.com', '$2y$10$/R/m0glBCxjkKYaAUs0M5uaSAJgn.lnPOhd7XizNCia/VydsFQ.qS', 'upload/avatar.png', '2020-03-11', '15:12:44', 3),
('Larissa', 'larry@gmail.com', '$2y$10$02/vlnKbOQ5RmrfrA5a.qequn2veLVXn1v21LBIaLxZdZM.RhFNna', 'upload/avatar.png', '2020-03-11', '16:59:14', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_videos`
--

CREATE TABLE `tb_videos` (
  `cod_video` int(11) NOT NULL,
  `nome_video` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `id_video` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `cod_categoria` int(11) NOT NULL,
  `data_videos` date NOT NULL,
  `hora_videos` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_videos`
--

INSERT INTO `tb_videos` (`cod_video`, `nome_video`, `id_video`, `cod_categoria`, `data_videos`, `hora_videos`) VALUES
(3, 'Dutra', '65YCvox3mFk', 0, '0000-00-00', '00:00:00'),
(4, 'BeibaDAADSAAD', 'YZScE0ohAy0', 0, '0000-00-00', '00:00:00'),
(7, 'GAARA VS LEE', 'VgDgWzBL7s4', 0, '0000-00-00', '00:00:00'),
(8, 'Gus', '3MxicuAGoxQ', 6, '2020-03-10', '15:13:53'),
(9, 'Lofi Guitar', 'o9Phw-cJqBQ', 6, '2020-03-10', '15:27:56'),
(10, 'The last of us 2', 'II5UsqP2JAk', 7, '2020-03-10', '15:28:59'),
(11, 'Silent Hillsss', 'r6NCC-nnvMU', 5, '2020-03-11', '16:49:32');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `tb_categoria`
--
ALTER TABLE `tb_categoria`
  ADD PRIMARY KEY (`cod_categoria`);

--
-- Índices para tabela `tb_usuario`
--
ALTER TABLE `tb_usuario`
  ADD PRIMARY KEY (`cod_usuario`);

--
-- Índices para tabela `tb_videos`
--
ALTER TABLE `tb_videos`
  ADD PRIMARY KEY (`cod_video`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `tb_categoria`
--
ALTER TABLE `tb_categoria`
  MODIFY `cod_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `tb_usuario`
--
ALTER TABLE `tb_usuario`
  MODIFY `cod_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `tb_videos`
--
ALTER TABLE `tb_videos`
  MODIFY `cod_video` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
